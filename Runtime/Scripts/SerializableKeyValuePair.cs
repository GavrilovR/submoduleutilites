﻿using System;
using UnityEngine;

namespace InternalAssets.Scripts.Utility
{
    [Serializable]
    public abstract class SerializableKeyValuePair<TKey, TValue>
    {
        [SerializeField] protected TKey key;
        [SerializeField] protected TValue value;

        public TKey Key => key;
        public TValue Value => value;
    }
}